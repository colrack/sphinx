#!/bin/python

import sys
import os
import string
import pdb

# Complete the function below.


def  isPangram( n):	
    """ Write you solution here """
    alphabet = list(string.ascii_lowercase)

    alphabet_list = list(alphabet)
    final_set = set()

    n = n.replace(" " , "").lower()

    chars = list(n)

    [final_set.add(x) for x in chars]


    final_list = list(final_set)

    final_list.sort()

    print alphabet_list
    print final_list

    return final_list == alphabet_list


def  isPangram2( n):
    """ Check if a input string pangram """

    # Create a set that contains ascii alphabet
    alphabet = set(string.ascii_lowercase)

    final_set = set()

    # Delete white chars from input
    n = n.replace(" " , "").lower()

    # Convert input string to char list to use in list compr
    chars = list(n)
    # For every input char add it to the set
    [final_set.add(x) for x in chars]

    # Check if final_set is a subset of alphabet
    u = alphabet.intersection(final_set)

    return u == alphabet


_n = "We promptly judged antique ivory buckles for the next prize"

_n2 = "We promptly judged antique ivory buckles for the net prize"

res = isPangram2(_n2);


def isPalindrome( n):
    


print res